//
//  CreateViewController.swift
//  WT
//
//  Created by RM on 28.12.2020.
//
import RealmSwift
import UIKit

class CreateViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var itemField: UITextField!
    @IBOutlet var amountField: UITextField!
    @IBOutlet var datePicker: UIDatePicker!
    
    
    
    private let realm = try! Realm()
    public var completionHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        itemField.becomeFirstResponder()
        itemField.delegate = self
        amountField.becomeFirstResponder()
        amountField.delegate = self
        
        datePicker.setDate(Date(), animated: true)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохранить", style: .done, target: self, action: #selector(didTapSaveButton))

    }
    
    override func viewWillAppear(_ animated: Bool) {
    AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        itemField.resignFirstResponder()
        amountField.resignFirstResponder()
        return true
    }
    
    @objc func didTapSaveButton() {
        let item = itemField.text!
        let amount = amountField.text!
        
        if !(item + amount).isEmpty {
            let date = datePicker.date
            
            realm.beginWrite()
            
            let newItem = WTItem()
            newItem.item = item
            newItem.amount = amount
            newItem.date = date
            realm.add(newItem)
            
            try! realm.commitWrite()
            
            completionHandler?()
            navigationController?.popToRootViewController(animated: true )
        }
        else {
            print("Добавьте что-нибудь ")
        }
        
    }
    
    
}
